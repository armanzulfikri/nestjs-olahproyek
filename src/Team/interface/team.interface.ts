import {Document } from 'mongoose';
import {Karyawan} from '../../Karyawan/interface/karyawan.interface'
import {Proyek} from '../../Proyek/interface/proyek.interface'

export interface Team extends Document {
    readonly proyek : Proyek;
    readonly karyawan : Karyawan;
    readonly peran : string;
  
}
