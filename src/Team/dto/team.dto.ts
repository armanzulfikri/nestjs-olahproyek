import {Document } from 'mongoose';

export class CreateTeamDto extends Document {
  readonly proyek : string;
  readonly karyawan : string;
  readonly peran : string;

}
