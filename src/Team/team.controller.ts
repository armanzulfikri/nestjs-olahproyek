import { Controller, Get, Post, Body, Delete,Put, Param, Res, HttpStatus, NotFoundException, Query } from '@nestjs/common';
import { TeamsService } from './team.service';
import { CreateTeamDto } from './dto/team.dto';
import { Team } from './interface/team.interface';

@Controller('teams')
export class TeamsController {
  constructor(private readonly teamService: TeamsService) {}

  @Post('/create')
  async create(@Res() res,@Body() CreateTeamDto: CreateTeamDto) {
    const data= await this.teamService.create(CreateTeamDto);
    return res.status (HttpStatus.OK).json({
      message : "Data Team suskses dibuat",data
    });
    
  }

  @Get('/all')
  async findAll(@Res() res) {
    const datas = await this.teamService.findAll();
    return res.status(HttpStatus.OK).json(datas);
  }
  @Get('/:teamId')
  async findById(@Res() res, @Param('teamId') teamId) {
    const lists = await this.teamService.findById(teamId);
    if (!lists) throw new NotFoundException('Id does not exist!');
    return res.status(HttpStatus.OK).json(lists);
  }

  @Put('/update/')
  async update(@Res() res, @Query('teamId') teamId, @Body() CreateTeamDto:CreateTeamDto) {
    const team = await this.teamService.update(teamId, CreateTeamDto);
    if (!team) throw new NotFoundException('Team does not exist!');
    return res.status(HttpStatus.OK).json({
        message: 'Team has been successfully updated',
        team
    });
}

  @Delete('delete/:teamId')
  async delete(@Param('teamId') teamId , @Res() res){
    const data = await this.teamService.delete(teamId);
    if (!data)throw new NotFoundException ('Data Team tidak ditemukan !');
    return res.status (HttpStatus.OK).json({
      message : 'Data Team Berhasil di Hapus',
      data
    });
  }


}
