import { Prop } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {KaryawanSchema} from '../../Karyawan/schema/karyawan.schema';
import {ProyekSchema} from '../../Proyek/schema/proyek.schema'

export const TeamSchema = new mongoose.Schema({
    proyek: {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'ProyekSchema',
        require :  true,
    },
    karyawan : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'KaryawanProyek',
        require :  true,
    },
    peran : {
        type : String,
        required : true,
    }
});
