import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateTeamDto } from './dto/team.dto';

import { Team} from './interface/team.interface';

@Injectable()
export class TeamsService {
  constructor(@InjectModel('Team') private readonly teamModel: Model<Team>) {}

  async create(CreateTeamDto:CreateTeamDto): Promise<Team> {
    const createTeam = new this.teamModel(CreateTeamDto);
    return createTeam.save();
  }

  async findAll(): Promise<Team[]> {
    return await this.teamModel.find().exec();
  }

  async findById(teamId): Promise <any>{
    const findOne = await this.teamModel.findById({_id:teamId}).exec();
    return findOne;
  }

  async update(teamId, CreateTeamDto: CreateTeamDto): Promise<Team> {
    const updateTeam = await this.teamModel
    .findByIdAndUpdate(teamId, CreateTeamDto, { new: true });
    return updateTeam
    }

    async delete(teamId): Promise<any> {
      return await this.teamModel.findByIdAndRemove({_id:teamId});
      }
}