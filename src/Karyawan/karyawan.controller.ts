import { Controller, Get, Post, Body, Delete,Put, Param, Res, HttpStatus, NotFoundException, Query } from '@nestjs/common';
import { KaryawansService } from './karyawan.service';
import { CreateKaryawanDto } from './dto/karyawan.dto';
import { Karyawan } from './interface/karyawan.interface';

@Controller('karyawans')
export class KaryawansController {
  constructor(private readonly karyawanService: KaryawansService) {}

  @Post('/create')
  async create(@Res() res,@Body() CreateKaryawanDto: CreateKaryawanDto) {
    const data= await this.karyawanService.create(CreateKaryawanDto);
    return res.status (HttpStatus.OK).json({
      message : "Data Karyawan suskses dibuat",data
    });
    
  }

  @Get('/all')
  async findAll(@Res() res) {
    const datas = await this.karyawanService.findAll();
    return res.status(HttpStatus.OK).json(datas);
  }
  @Get('/:karyawanId')
  async findById(@Res() res, @Param('karyawanId') karyawanId) {
    const lists = await this.karyawanService.findById(karyawanId);
    if (!lists) throw new NotFoundException('Id does not exist!');
    return res.status(HttpStatus.OK).json(lists);
  }

  @Put('/update/')
  async update(@Res() res, @Query('karyawanId') karyawanId, @Body() CreateKaryawanDto:CreateKaryawanDto) {
    const karyawan = await this.karyawanService.update(karyawanId, CreateKaryawanDto);
    if (!karyawan) throw new NotFoundException('Karyawan does not exist!');
    return res.status(HttpStatus.OK).json({
        message: 'Karyawan has been successfully updated',
        karyawan
    });
}

  @Delete('delete/:karyawanId')
  async delete(@Param('karyawanId') karyawanId , @Res() res){
    const data = await this.karyawanService.delete(karyawanId);
    if (!data)throw new NotFoundException ('Data Karyawan tidak ditemukan !');
    return res.status (HttpStatus.OK).json({
      message : 'Data Karyawan Berhasil di Hapus',
      data
    });
  }


}
