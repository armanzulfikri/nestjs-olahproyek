import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { KaryawansController } from './karyawan.controller';
import { KaryawansService } from './karyawan.service';
import { KaryawanSchema } from './schema/karyawan.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Karyawan', schema: KaryawanSchema }])],
  controllers: [KaryawansController],
  providers: [KaryawansService],
  exports: [KaryawansService],
})
export class KaryawansModule {}
