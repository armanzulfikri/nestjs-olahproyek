import * as mongoose from 'mongoose';

export const KaryawanSchema = new mongoose.Schema({
  nama: String,
  no_pegawai: Number,
  tanggal_mulai : Date,
  tanggal_akhir : Date,
});
