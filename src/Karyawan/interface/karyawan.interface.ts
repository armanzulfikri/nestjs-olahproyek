import {Document, Number } from 'mongoose';

export interface Karyawan extends Document {
  readonly nama: string;
  readonly no_pegawai: number;
  readonly tanggal_mulai : Date;
  readonly tanggal_akhir : Date;
}
