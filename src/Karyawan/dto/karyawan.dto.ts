import {Document } from 'mongoose';

export class CreateKaryawanDto extends Document {
  readonly nama: string;
  readonly no_pegawai: number;
  readonly tanggal_mulai : Date;
  readonly tanggal_akhir : Date;
}
