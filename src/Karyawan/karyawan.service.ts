import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateKaryawanDto } from './dto/karyawan.dto';

import { Karyawan} from './interface/karyawan.interface';

@Injectable()
export class KaryawansService {
  constructor(@InjectModel('Karyawan') private readonly karyawanModel: Model<Karyawan>) {}

  async create(CreateKaryawanDto:CreateKaryawanDto): Promise<Karyawan> {
    const createKaryawan = new this.karyawanModel(CreateKaryawanDto);
    return createKaryawan.save();
  }

  async findAll(): Promise<Karyawan[]> {
    return await this.karyawanModel.find().exec();
  }

  async findById(karyawanId): Promise <any>{
    const findOne = await this.karyawanModel.findById({_id:karyawanId}).exec();
    return findOne;
  }

  async update(karyawanId, CreateKaryawanDto: CreateKaryawanDto): Promise<Karyawan> {
    const updateKaryawan = await this.karyawanModel
    .findByIdAndUpdate(karyawanId, CreateKaryawanDto, { new: true });
    return updateKaryawan
    }

    async delete(karyawanId): Promise<any> {
      return await this.karyawanModel.findByIdAndRemove({_id:karyawanId});
      }
}