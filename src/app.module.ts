import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { KaryawansModule } from './Karyawan/karyawan.module';
import {ProyeksModule} from './Proyek/proyek.module'
import { TeamsModule } from './Team/team.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb+srv://arman:root@cluster0.nqycw.mongodb.net/olahproyek?retryWrites=true&w=majority',
      { useNewUrlParser: true, useUnifiedTopology: true },
    ),
    KaryawansModule,ProyeksModule,TeamsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
