import { Controller, Get, Post, Body, Delete,Put, Param, Res, HttpStatus, NotFoundException, Query } from '@nestjs/common';
import { ProyeksService } from './proyek.service';
import { CreateProyekDto } from './dto/proyek.dto';
import { Proyek } from './interface/proyek.interface';

@Controller('proyeks')
export class ProyeksController {
  constructor(private readonly proyekService: ProyeksService) {}

  @Post('/create')
  async create(@Res() res,@Body() CreateProyekDto: CreateProyekDto) {
    const data= await this.proyekService.create(CreateProyekDto);
    return res.status (HttpStatus.OK).json({
      message : "Data Proyek suskses dibuat",data
    });
    
  }

  @Get('/all')
  async findAll(@Res() res) {
    const datas = await this.proyekService.findAll();
    return res.status(HttpStatus.OK).json(datas);
  }
  @Get('/:proyekId')
  async findById(@Res() res, @Param('proyekId') proyekId) {
    const lists = await this.proyekService.findById(proyekId);
    if (!lists) throw new NotFoundException('Id does not exist!');
    return res.status(HttpStatus.OK).json(lists);
  }

  @Put('/update/')
  async update(@Res() res, @Query('proyekId') proyekId, @Body() CreateProyekDto:CreateProyekDto) {
    const proyek = await this.proyekService.update(proyekId, CreateProyekDto);
    if (!proyek) throw new NotFoundException('Proyek does not exist!');
    return res.status(HttpStatus.OK).json({
        message: 'Proyek has been successfully updated',
        proyek
    });
}

  @Delete('delete/:proyekId')
  async delete(@Param('proyekId') proyekId , @Res() res){
    const data = await this.proyekService.delete(proyekId);
    if (!data)throw new NotFoundException ('Data Proyek tidak ditemukan !');
    return res.status (HttpStatus.OK).json({
      message : 'Data Proyek Berhasil di Hapus',
      data
    });
  }


}
