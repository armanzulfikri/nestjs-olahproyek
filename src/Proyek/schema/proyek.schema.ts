import * as mongoose from 'mongoose';

export const ProyekSchema = new mongoose.Schema({
    judul_proyek: String,
    nama_klien :String,
    kontak_klien : Number,
    status : String,
    tanggal_mulai : Date,
    tanggal_tenggat : Date,
});
