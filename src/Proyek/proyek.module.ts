import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ProyeksController } from './proyek.controller';
import { ProyeksService } from './proyek.service';
import { ProyekSchema } from './schema/proyek.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Proyek', schema: ProyekSchema }])],
  controllers: [ProyeksController],
  providers: [ProyeksService],
  exports: [ProyeksService],
})
export class ProyeksModule {}
