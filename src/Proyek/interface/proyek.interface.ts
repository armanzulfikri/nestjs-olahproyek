import {Document } from 'mongoose';

export interface Proyek extends Document {
    readonly judul_proyek: string;
    readonly nama_klien :string;
    readonly kontak_klien : number;
    readonly status : string;
    readonly tanggal_mulai : Date;
    readonly tanggal_tenggat : Date;
}
