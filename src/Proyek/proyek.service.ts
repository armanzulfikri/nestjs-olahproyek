import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateProyekDto } from './dto/proyek.dto';

import { Proyek} from './interface/proyek.interface';

@Injectable()
export class ProyeksService {
  constructor(@InjectModel('Proyek') private readonly proyekModel: Model<Proyek>) {}

  async create(CreateProyekDto:CreateProyekDto): Promise<Proyek> {
    const createProyek = new this.proyekModel(CreateProyekDto);
    return createProyek.save();
  }

  async findAll(): Promise<Proyek[]> {
    return await this.proyekModel.find().exec();
  }

  async findById(proyekId): Promise <any>{
    const findOne = await this.proyekModel.findById({_id:proyekId}).exec();
    return findOne;
  }

  async update(proyekId, CreateProyekDto: CreateProyekDto): Promise<Proyek> {
    const updateProyek = await this.proyekModel
    .findByIdAndUpdate(proyekId, CreateProyekDto, { new: true });
    return updateProyek
    }

    async delete(proyekId): Promise<any> {
      return await this.proyekModel.findByIdAndRemove({_id:proyekId});
      }
}